require("dotenv").config();
require("./conexion");
import express from "express";
import { dbController } from "./controllers/dbController";
import { MutationController } from "./controllers/mutationController";

const app = express();
app.use(express.json());

app.post("/mutation", (req, response) => {
  let adn: String[] = req.body.dna;// se obtiene la cadena de ADN desde el body

  if (MutationController.esAdnValido(adn)) { // se verifica que sea valido
    var esMutacion: Boolean = MutationController.hasMutation(adn); // se analiza la cadena en busqueda de mutaciones
    dbController.guardarADN(adn, esMutacion); //  se almacena en la base de datos el resultado
    MutationController.hasMutation(req.body.dna) ? response.status(200).send() : response.status(403).send();// se responde true = status 200,  false = status 403
  } else {
    response
      .status(403)
      .send({ mensaje: "Verifíque que envió como parámetro un array de Strings que representan cada fila de una tabla (NxN) y que además Las letras de los Strings solo sean: (A,T,C,G)" });
  }
});

app.get("/stat", async (_req, response) => {
response.status(200).send(await dbController.statADN())
});

app.listen(process.env.PORT, () => {
  console.log(`API corriendoen el puerto ${process.env.PORT}`);
});

export default app;


