require("dotenv").config();
import mongoose, { model, Model, Schema } from "mongoose";

var MONGO_URI = `mongodb://${process.env.IP}/${process .env.DB}`;

mongoose
  .connect(MONGO_URI, {})
  .then(() => console.log("conectado a mongo"))
  .catch((e) => console.log(e));
