export class MutationController {
  public static hasMutation(adn: String[]): boolean {
    // Acumula total de mutaciones encontradas
    var totalMutaciones: number = 0;

    for (let i = 0; i < adn.length; i++) {
      for (let j = 0; j < adn[i].length; j++) {
        // Verificacion Horizontal
        if (j < adn[i].length - 3) {
          if (this.sonIguales(adn[i].charAt(j), adn[i].charAt(j + 1), adn[i].charAt(j + 2), adn[i].charAt(j + 3))) {
            totalMutaciones++;
          }
        }
        // Verificacion Vertical
        if (i < adn.length - 3) {
          if (this.sonIguales(adn[i].charAt(j), adn[i + 1].charAt(j), adn[i + 2].charAt(j), adn[i + 3].charAt(j))) {
            totalMutaciones++;
          }
        }

        //Verificacion Diagonal
        if (i < adn.length - 3 && j < adn[i].length - 3) {
          if (this.sonIguales(adn[i].charAt(j), adn[i + 1].charAt(j + 1), adn[i + 2].charAt(j + 2), adn[i + 3].charAt(j + 3))) {
            totalMutaciones++;
          }
        }

        //Verificacion Contradiagonal
        if (i >= 3 && j < adn[i].length - 3) {
          if (this.sonIguales(adn[i].charAt(j), adn[i - 1].charAt(j + 1), adn[i - 2].charAt(j + 2), adn[i - 3].charAt(j + 3))) {
            totalMutaciones++;
          }
        }
      }
    }
    if (totalMutaciones > 1) {
      return true;
    }
    return false;
  }

  public static sonIguales(a: String, b: String, c: String, d: String): boolean {
    return a == b && b == c && c == d;
  }

  public static esAdnValido(adn:String[]):boolean {

    for (let i = 0; i < adn.length; i++) {
        // si la matriz tiene menos de 4 filas o columnas o no es NxN retorna falso
        if (adn[i].length < 4 || adn.length < 4 || adn[i].length != adn.length) {
            return false;
        }
        //Expresion Regular, si posee una letra que no sea ATCG retorana falso
        if (adn[i].match(".*[^ATCG].*")) {
            return false;
        }
    }

    return true;
}
}
