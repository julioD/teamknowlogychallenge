const Adn = require("../models/adn");

export class dbController {
  public static guardarADN(adn: String[], esMutacion: Boolean): void {
    const nuevoADN = new Adn({ _id: adn.toString(), mutacion: esMutacion });
    console.log(nuevoADN);

    nuevoADN.save((err: any, document: any) => {
      if (err) console.log(err);
      console.log(document);
    });
  }

  public static async statADN() {
    let count_mutations = 0;
    let count_no_mutation = 0

    await Promise.all([
        Adn.count({mutacion:true}),
        Adn.count({mutacion:false})
      ]).then( ([ mutaciones, noMutaciones ]) => {

        count_mutations = mutaciones
        count_no_mutation = noMutaciones

      });

      let ratio = count_mutations / count_no_mutation
      return {"count_mutations": count_mutations, "count_no_mutation": count_no_mutation,"ratio":ratio}

  }
}
