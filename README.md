# Back-End teamknowlogy challenge

Desarrollo de un proyecto (en node) que detecta si una persona tiene diferencias genéticas basándose en su secuencia de ADN.
Posee un servicio que responde con el siguiente metodo

boolean hasMutation( String[] dna ); // Ejemplo Java

Recibe como parámetro un array de Strings que representan cada fila de una tabla
de (NxN) con la secuencia del ADN. Las letras de los Strings solo pueden ser: (A,T,C,G), las
cuales representa cada base nitrogenada del ADN.

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._
```
1) Crea una directorio para descargar el proyecto
2) Utiliza git dentro del directorio para clonar el proyecto : git clone https://gitlab.com/julioD/teamknowlogychallenge.git
```
### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
git 
nodejs v16.15.1
npm v8.11.0
MongoDB Community Server v5.0.9
```
### Instalación de MongoDB Community Server v5.0.9 🔧

_Un ejemplo paso a paso que te dice lo que debes ejecutar para tener MongoDB Community Server v5.0.9 ejecutandose_

```
1) Descargar e instalar MongoDB Community Server v5.0.9
2) Agregar 'C:\Program Files\MongoDB\Server\5.0\bin' al path en las variables de entorno de SO
3) Crear el directorio 'C:\data\db'
4) Ejecutar por terminal el comando 'mongod' para ejecutar el servicio de MongoDB
```

### Instalación del sistema teamknowlogyChallege 🔧

_Un ejemplo paso a paso que te dice lo que debes ejecutar para tener un entorno de desarrollo ejecutandose de forma local_

```
1) Una vez descargado el proyecto abre la carpeta teamknowlogyChallenge con tu editor de código favorito
2) Abre una consola de comandos en la direccion del proyecto
3) Ejecuta el comando 'npm install' para instalar todas las dependecias del proyecto
4) Ejecuta el script 'npm run dev' para lanzar el proyecto (asegúrate de tener ejecutado el servicio de MongoDB)
```

_En este punto tendras una el sistema corriendo en tu servidor local en el puerto 3000.
Podras ejecutar las siguientes consultas:_
```diff
- POST: http://localhost:3000/mutation body ej: {"dna":["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]}
- GET: http://localhost:3000/stat
```
```diff
+ Adicionalmente podras realizar las mismas consultas a la instancia del sistema teamknowlogyChallege que se encuentra corriendo en AWS
```
```diff
- POST: http://34.237.60.52/mutation body ej: {"dna":["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]}
- GET: http://34.237.60.52/stat
```

## Ejecutando las pruebas ⚙️

_Para ejecutar las pruebas automaticas deberas ejecutar el script_
```
npm run test
```
_Podrás observar el siguiente resultado_

![DC](https://gitlab.com/julioD/teamknowlogychallenge/-/raw/main/test.png)


## Construido con 🛠️

_Herramientas utilizadas para desarrollar el proyecto_
```
  "dependencies": {
    "express": "^4.18.1",
    "mongoose": "^6.4.0",
    "supertest": "^6.2.3"
  },
    "devDependencies": {
    "@types/express": "^4.17.13",
    "@types/jest": "^28.1.2",
    "@types/supertest": "^2.0.12",
    "dotenv": "^16.0.1",
    "jest": "^28.1.1",
    "nodemon": "^2.0.16",
    "ts-jest": "^28.0.5",
    "ts-node-dev": "^2.0.0",
    "typescript": "^4.7.4"
  }
```

## Autor ✒️

D'Orazi julio cesar - [Gitlab](https://gitlab.com/julioD)
                    - [linkedin](https://www.linkedin.com/in/dorazi-julio-cesar/)
                    **Gmail:** dorazijulioc@gmail.com

