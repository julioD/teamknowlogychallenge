"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.dbController = void 0;
const Adn = require("../models/adn");
class dbController {
    static guardarADN(adn, esMutacion) {
        const nuevoADN = new Adn({ _id: adn.toString(), mutacion: esMutacion });
        console.log(nuevoADN);
        nuevoADN.save((err, document) => {
            if (err)
                console.log(err);
            console.log(document);
        });
    }
    static statADN() {
        return __awaiter(this, void 0, void 0, function* () {
            let count_mutations = 0;
            let count_no_mutation = 0;
            yield Promise.all([
                Adn.count({ mutacion: true }),
                Adn.count({ mutacion: false })
            ]).then(([mutaciones, noMutaciones]) => {
                count_mutations = mutaciones;
                count_no_mutation = noMutaciones;
            });
            let ratio = count_mutations / count_no_mutation;
            return { "count_mutations": count_mutations, "count_no_mutation": count_no_mutation, "ratio": ratio };
        });
    }
}
exports.dbController = dbController;
