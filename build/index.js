"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv").config();
require("./conexion");
const express_1 = __importDefault(require("express"));
const dbController_1 = require("./controllers/dbController");
const mutationController_1 = require("./controllers/mutationController");
const app = (0, express_1.default)();
app.use(express_1.default.json());
app.post("/mutation", (req, response) => {
    let adn = req.body.dna; // se obtiene la cadena de ADN desde el body
    if (mutationController_1.MutationController.esAdnValido(adn)) { // se verifica que sea valido
        var esMutacion = mutationController_1.MutationController.hasMutation(adn); // se analiza la cadena en busqueda de mutaciones
        dbController_1.dbController.guardarADN(adn, esMutacion); //  se almacena en la base de datos el resultado
        mutationController_1.MutationController.hasMutation(req.body.dna) ? response.status(200).send() : response.status(403).send(); // se responde true = status 200,  false = status 403
    }
    else {
        response
            .status(403)
            .send({ mensaje: "Verifíque que envió como parámetro un array de Strings que representan cada fila de una tabla (NxN) y que además Las letras de los Strings solo sean: (A,T,C,G)" });
    }
});
app.get("/stat", (_req, response) => __awaiter(void 0, void 0, void 0, function* () {
    response.status(200).send(yield dbController_1.dbController.statADN());
}));
app.listen(process.env.PORT, () => {
    console.log(`API corriendoen el puerto ${process.env.PORT}`);
});
