"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
var adnSchema = new mongoose_1.Schema({
    _id: String,
    mutacion: Boolean
});
module.exports = (0, mongoose_1.model)('Adn', adnSchema);
