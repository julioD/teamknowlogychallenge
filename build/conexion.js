"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv").config();
const mongoose_1 = __importDefault(require("mongoose"));
var MONGO_URI = `mongodb://${process.env.IP}/${process.env.DB}`;
mongoose_1.default
    .connect(MONGO_URI, {})
    .then(() => console.log("conectado a mongo"))
    .catch((e) => console.log(e));
