import app from '../src/index';
import request from "supertest";


describe('POST /mutation',()=>{
    test('debería responder con un codigo de estado 403 Forbiden',async ()=>{
        var dnaFalse = {"dna":["ATGCGA","CAGTGC","TTATTT","AGACGG","GCGTCA","TCACTG"]} // No contiene mutacion
        await request(app).post('/mutation').send(dnaFalse).expect(403) //  Se espera un codigo de estado 403 Forbiden
    })
});


describe('POST /mutation',()=>{
    test('debería responder con un codigo de estado 200 OK',async ()=>{
        var dnaTrue = {"dna":["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]} //  Contiene mutacion
        await request(app).post('/mutation').send(dnaTrue).expect(200) //  Se espera un codigo de estado 200 OK
    })
});

describe('POST /mutation',()=>{
    test('debería responder un mensaje de formato de adn inválido',async ()=>{
        var dnaFalse = {"dna":["PPPPPP","PPPPPP","PPPPPP","PPPPPP","PPPPPP","PPPPPP"]} // Contiene letra no permitida
        await request(app).post('/mutation').send(dnaFalse).expect({mensaje: 'Verifíque que envió como parámetro un array de Strings que representan cada fila de una tabla (NxN) y que además Las letras de los Strings solo sean: (A,T,C,G)'})
    })
});

describe('POST /mutation',()=>{
    test('debería responder un mensaje de formato de adn inválido',async ()=>{
        var dnaFalse = {"dna":["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA"]}  // la matriz no es NxN
        await request(app).post('/mutation').send(dnaFalse).expect({mensaje: 'Verifíque que envió como parámetro un array de Strings que representan cada fila de una tabla (NxN) y que además Las letras de los Strings solo sean: (A,T,C,G)'})
    })
});

